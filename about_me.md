# Nolan Sherman

![image](https://gitlab.com/nolanrsherman/flutter-intro/uploads/e4ad1748d8791c68c262dea3c86367a1/image.png)

## Nolan Sherman
### Gratudated UNO CSCI 2020

- nsherma1@uno.edu
- https://www.linkedin.com/in/nolanrsherman/
#

## During My Undergrad
### - ICPC
### - IGDA President
### - ACM Co-President
### - Internship with Susco Solutions

#
## Now
### SpotOn - Full Stack Developer on the Restaurant Point-of-Sale Team
- golang
- react
- cloud 

### City Lead for FaithTech New Orleans
weekly and monthly meetups. https://faithtech.com/new-orleans 
Projects for:
 - help our local community
 - disadvantaged people
 - tools that help faith based organizations do the above ^

### AAS (Always another Startup 😂)
- Malady
- Consulting
- A social marketing app to connect businesses and customers